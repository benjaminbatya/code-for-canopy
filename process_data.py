#!/usr/bin/python3
__author__ = 'benjamin'

## Begin utility functions

from util_funcs import *
import numpy as np
import xml.etree.ElementTree as ET
import pickle
import urllib.request
import urllib.parse
import time
import argparse
import os

class Tree(object):
    def __init__(self):
        self.name = None            # Usually the street address of the tree
        self.site = None            # There could be more then one tree at a location

        # self.side = None            # Random data attached to the tree
        self.species = None         # species of the tree
        self.date_planted = None    # Date the tree is planted on

        self.coordinates = None     # The final coordinates of the tree

        self.city_state = None      # The city and state this tree belongs to
        self.geocoding = None     # The returned geocoding of this tree

        self.region = None          # The region that this tree belongs to

    def __str__(self):
        ret = \
        "Name: %s, Coordinates: %s, Site: %s, Side: %s, Species: %s, Date Planted: %s" % \
        ( self.name, self.coordinates, self.site, self.side, self.species
        , self.date_planted) #,  self.dbh, self.x, self.y, self.geom)
        #, DBH: %s, X: %s, Y: %s, Geom: %s" % \
        return ret

def find_data(elem, data_str, data_name, ns, required, tree_name):
    try_to_find = elem.find(data_str.format(data_name), ns)
    if try_to_find is not None:
        if(try_to_find.text is None):
            # NOTE: this can happen if the element is empty
            return ""
        return try_to_find.text.strip()
    elif required:
        ERROR("find_data: in element {}: failed to find required datum '{}'".format(tree_name, data_name))

    return None

class DataMapElem:
    def __init__(self, xml_attr, data_attr, required):
        self.xml_attr = xml_attr
        self.data_attr = data_attr
        self.required = required

def parse_trees(ns, root):
    INFO("Parsing trees")

    # Find all of the properly formatted trees
    tree_elems = root.findall('.//gis:Placemark[gis:Point]', ns) # [gis:Point]', ns)
    # Find all of the added on, extra trees
    tree_elems_extra = root.findall('.//gis:Placemark[gis:address]', ns)
    # Put them together
    tree_elems += tree_elems_extra

    # INFO(len(trees))
    trees = []
    for elem in tree_elems:
        # INFO(elem)
        tree = Tree()
        trees.append(tree)

        tree.name = elem.find('./gis:name', ns).text.strip()

        data_str = './gis:ExtendedData/gis:Data[@name="{}"]/gis:value'

        tree.site = find_data(elem, data_str, 'Side/Site', ns, True, tree.name) # elem.find(data_str.format('SITE'), ns).text
        tree.species = find_data(elem, data_str, 'Species', ns, False, tree.name) # elem.find(data_str.format('SPECIES'), ns).text

        tree.date_planted = find_data(elem, data_str, 'Complete Date', ns, False, tree.name)

        lat = find_data(elem, data_str, 'Lat', ns, True, tree.name)
        lng = find_data(elem, data_str, 'Long', ns, True, tree.name)

        if lat and lng:
            tree.coordinates = np.array((float(lng), float(lat)))

    return trees

def request_geocode_trees(trees):
    INFO("Geocoding the trees with missing gps locations")
    # Figure out the GPS coordinates using http://maps.googleapis.com/maps/api/geocode
    # import json
    url = 'http://maps.googleapis.com/maps/api/geocode/xml'
    idx = 0
    for tree in trees:
        # Skip the tree if we already have it's gps coordinates
        if tree.coordinates is not None:
            continue

        print("request_geocode_trees: tree '%s': requesting geocode" % tree.name)

        # Have to respect google's usage limits:
        # https://developers.google.com/maps/documentation/geocoding/#Limits
        if(idx%5 == 4): time.sleep(1)
        idx += 1

        # city_state = tree.city_state
        # if city_state is None:
        city_state = "Palo Alto, CA"

        full_address = tree.name + ", " + city_state
        params = {'address' : full_address }
        params_str = urllib.parse.urlencode(params)
        full_url = url + '?' + params_str
        request = urllib.request.Request(full_url)
        response = urllib.request.urlopen(request)
        data = response.read().decode()
        tree.geocoding = data
    return trees

def process_tree_geocoding(trees):
    for tree in trees:
        if tree.geocoding is None:
            continue

        print("process_tree_geocoding: tree '%s': converting geocoding to coordinates" % tree.name)

        root = ET.fromstring(tree.geocoding)
        # INFO(ET.tostring(root).decode())
        lng = root.find('.//location/lng').text
        lat = root.find('.//location/lat').text
        tree.coordinates = np.array((float(lng), float(lat)))
    # INFO([(tree.full_address, tree.coordinates) for tree in trees])
    return trees

# From https://github.com/tparkin/Google-Maps-Point-in-Polygon/blob/master/maps.google.polygon.containsLatLng.js
def check_point_in_poly(pt, poly):
    in_poly = False

    num = poly.shape[0]
    j = num-1
    for i in range(num):
        vert1 = poly[i]
        vert2 = poly[j]

        if( (vert1[0] < pt[0] and pt[0] <= vert2[0]) or (vert2[0] < pt[0] and pt[0] <= vert1[0]) ):

            if(vert1[1] + (pt[0] - vert1[0]) / (vert2[0] - vert1[0]) * (vert2[1] - vert1[1])) < pt[1]:
                in_poly = not in_poly

        j = i

    return in_poly

def classify_trees_by_regions(trees, regions):
    for tree in trees:
        in_regions = []
        # INFO("tree at %r" % tree.coordinates)
        for region in regions:
            # INFO(region.coordinates)
            if check_point_in_poly(tree.coordinates, region.coordinates):
                in_regions.append(region)

        if(len(in_regions) == 0):
            ERROR("Tree '%s:%s' is not in any region!" % (tree.name, tree.site))
        else:
            if(len(in_regions) > 1):
                ERROR("Tree '%s:%s' is in multiple regions. Adding it to the first region!" % (tree.name, tree.site))

            tree.region = in_regions[0]
            # INFO("Tree %r in region %r" % (tree.street, tree.region.name))

def write_out_trees(writer, trees):
    count = 0
    writer.writerow(["Name", "Region", "Longitude", "Latitude",
                     "Site", "Species", "Date Planted"])
    for tree in trees:
        region = tree.region.name if tree.region else None
        writer.writerow([tree.name, region,
                         tree.coordinates[0], tree.coordinates[1],
                         tree.site, tree.species, tree.date_planted])
        count += 1
    return count

def write_out_trees_by_region(writer, region, trees):
    count = 0
    writer.writerow(["Name", "Region", "Longitude", "Latitude",
                     "Site", "Species", "Date Planted"])
    for tree in trees:
        if region == tree.region:
            writer.writerow([tree.name, region.name if region else None,
                             tree.coordinates[0], tree.coordinates[1],
                             tree.site, tree.species, tree.date_planted])
            count += 1
    return count

class Region(object):
    def __init__(self):
        self.name = None
        self.coordinates = None

    def __str__(self):
        ret = "Name: %s, Coordinates: %s" % (self.name, self.coordinates)
        return ret

def parse_regions(ns, root):
    INFO("Parsing the regions")
    region_elems = root.findall('.//gis:Placemark[gis:Polygon]', ns)
    regions = []
    for elem in region_elems:
        # INFO(elem)
        region = Region()
        regions.append(region)

        region.name = elem.find('./gis:name', ns).text
        coord_str = elem.find('./gis:Polygon/gis:outerBoundaryIs/gis:LinearRing/gis:coordinates', ns).text
        coord_str = coord_str.strip()
        coord_str = coord_str.split()
        coords = []
        for c in coord_str:
            (lng, lat, alt) = c.split(',')
            pair = (float(lng), float(lat))
            coords.append(pair)
        region.coordinates = np.array(coords)

    return regions

def main():

    # Handle the argument parsing
    # From https://docs.python.org/2/library/argparse.html#module-argparse
    parser = argparse.ArgumentParser(description="Process a map file")
    parser.add_argument('map', help='Map file to parse')
    args = parser.parse_args()

    map_path = args.map

    INFO("Pasring map %s" % map_path)
    # INFO(sys.argv)

    model = None

    doc = ET.parse(map_path)
    root = doc.getroot()

    ns = {'gis' : 'http://www.opengis.net/kml/2.2'}

    # Parse out the trees
    trees = parse_trees(ns, root)

    # Parse out the regions
    regions = parse_regions(ns, root)

    # NOTE: geocoding is not required for the 2016 data
    # Get the geocoding for trees that need it
    # request_geocode_trees(trees)
    # Convert the geocoding to floats
    # process_tree_geocoding(trees)

    # Do the actual classification
    classify_trees_by_regions(trees, regions)

    CSV_EXT = "csv"
    FILE_SEP = "_"

    (root, ext) = os.path.splitext(map_path)
    csv_path = root + os.path.extsep + CSV_EXT

    # Write out the spreadsheet to csv format
    import csv
    with open(csv_path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        num_trees = write_out_trees(writer, trees)
        INFO("Saving {} trees to '{}'".format(num_trees, csv_path))

    # INFO(model)

    region_tree_count_arr = [0] * (len(regions)+1)
    region_idx = 0
    for region in regions:
        csv_path = root + FILE_SEP + region.name + os.path.extsep + CSV_EXT
        with open(csv_path, 'w') as csvfile:
            writer = csv.writer(csvfile)
            region_tree_count_arr[region_idx] = write_out_trees_by_region(writer, region, trees)
            INFO("Saving {} grouped trees to '{}'".format(region_tree_count_arr[region_idx], csv_path))
            region_idx += 1

    csv_path = root + FILE_SEP + 'ungrouped' + os.path.extsep + CSV_EXT
    with open(csv_path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        region_tree_count_arr[region_idx] = write_out_trees_by_region(writer, None, trees)
        INFO("Saved {} ungrouped trees to '{}'".format(region_tree_count_arr[region_idx], csv_path))
        region_idx += 1

    region_tree_count = sum(region_tree_count_arr)

    if(num_trees != region_tree_count):
        ERROR("PROBLEM!!! num_trees = {} != region_tree_count={}".format(num_trees, region_tree_count))


if __name__ == "__main__":
    main()
